**1) Your email**

Go to https://zimbra.univ-grenoble-alpes.fr and then log with your AGALAN = UGA login and password.  if not working, send an email to isterre-rh@univ-grenoble-alpes.fr

**2) The intranet**

Visit https://isterre-intranet.osug.fr with your AGALAN = UGA account. if not working, send an email to isterre-rh@univ-grenoble-alpes.fr

- look at the (EN or FR) 'IT INFORMATION' and more specifically: 
- look at the IT INFORMATION / computational machines, the printer also.
- look at the 'IT INFORMATION / Email lists' and register yourself to isterre-etudiants-cycle (or the one corresponding) as well as isterre-calcul
- if you need to ask something to the isterre IT, send a ticket to isterre-sos@univ-grenoble-alpes.fr . There are no stupid questions, as long as you are nice, polite and you thank them after.

**3) Computational machines =  cluster**

- ISTOAR: there is the one inside ISTerre, 'ISTOAR', for logging, etc, look at the ISTerre intranet IT INFORMATION / computational machines.
- GRICAD: then, there are the ones of UGA, handled by GRICAD. It consists of the clusters Dahu and BigFoot (and another one). These are the names of different 'clusters', each one having a lot of computational nodes, and each one being specialized for a type of usage. Bigfoot is dedicated to GPU.
1. documentation of Gricad HPC (FR or EN) : https://gricad-doc.univ-grenoble-alpes.fr/en (also, I add here some links to docs for GRICAD usage from other labs than ISTerre, that can have more details but are not specific to our usage: https://iab-env-epi.gricad-pages.univ-grenoble-alpes.fr/ciment-user-guide and https://scalde.gricad-pages.univ-grenoble-alpes.fr/web/pages/clusters.html#clusters )
2. start reading from the beginning, and get an account on Perseus. Here, you need to ask to join the project called 'pr-ml-remotesens' and I will accept you.
3. read carefully and do all the steps in 'HPC' 0., 1. and 4. (for 4., read+do the steps 'Job execution' and 'Job management') ! maybe, for logging, you will need to use directly 'ssh dahu' and not 'ssh dahu@ujf...'
4. If using (or will use) GPUs, use BigFoot.
- JEAN ZAY: Last, there is a national cluster that we can have access for IA development. I have a new account, in case you need, just come to me. 
	
**4) Data management**
- your computer: set up a backuppc if possible by asking the it services at isterre. look at the page intranet 'IT INFORMATION / backups and security', and ask the isterre IT if needed. Otherwise, at least use git (such as gitlab, see below) for saving your codes.
- CODE storage on the clusters (both clusters, ISTerre or GRICAD) once you are logged in : use the /home/yourlogin storage location for the codes. It is only visible by you and is BACKED-UP. But it is small, so DON'T STORE DATA THERE.
- DATA and PROCESSING/RESULTS storage for the clusters: 
1. ISTOAR: visit https://isterre-intranet.osug.fr/spip.php?article200.

      In /data/team/login (team=cycle for ex.) for the important files 

      In /nfs_scratch/login for the ones not needed to be backed up
2. GRICAD: visit https://gricad-doc.univ-grenoble-alpes.fr/en/hpc/data_management/

      In /bettik/login but NOT BACKED UP (even if it is not really a 'scratch')
3. For both: SUMMER is a backed up storage accessible from both istoar and (most of) Gricad, but very expensive to the team. Store here only the very important data or results. You need to ask us to get the permission.
      From Gricad (from Dahu for ex.): /summer/cycle2/yourlogin

**5) Interacting together**

On top of real meeting and emails, I encourage you to get a slack account here https://join.slack.com/t/isterre-cycle/shared_invite/zt-2vi2bkrhf-1QoQ~40GYHMIava79pLd7g (if the link does not work, send an email to sophie.giffard [at] univ-grenoble-alpes.fr) so we can: discuss one-to-one, make a channel with the different persons of your project, and also discuss/create channels with other students (even private ones).

**6) Gitlab: store and share your codes**

I invite you to have a folder 'codes' in your computer, a folder 'data' (and for ex. a folder 'results'). But keep the codes separated. Then, you can create a repository on gitlab (or github if you prefer, but UGA gitlab is best, see below the link) with your 'codes' folder  and add your supervisors as collaborators. The advantage is: i) to store all the versions of your codes, even when you are gone ii) to be able to easily discuss it and interact iii) to develop from your computer and have a cloned version in the cluster iv) to be able to add it in your cv later. Don't be scared to have bad/incomplete/.. codes. We are doing research, we are not experts in coding (yet)! 

Log in here: https://gricad-gitlab.univ-grenoble-alpes.fr and make your repository (ask if you need to make more than one as there is a student limit but we can overcome it). !! use the specific UGA gitlab and do not type 'gitlab' on Google. the address should start with 'gricad-gitlab..' 


**7) Access to publications**

You can use Google Scholar to search/find interesting papers related to your subject. Whenever you cannot have access to the pdf, use this link: https://sid2nomade-1.grenet.fr/login?url=http://scholar.google.com and log it before searching again. You will then have access to all the journals that UGA is registered to.

**8) Tools**

- **Copying from the clusters**: if you need to copy/paste from your computer to a cluster or vice versa, use the `scp` command. For Gricad, look here to first set the access host transparent: https://gricad-doc.univ-grenoble-alpes.fr/en/hpc/connexion/#making-the-access-host-transparent-using-ssh-proxycommands

1. Example of scp command for istoar: `scp -r your_login@ist-oar.ujf-grenoble.fr:/data/cycle/your_login/your_folder/ /folder_on_your_computer_to_paste/`

2. Example of scp command for gricad: `scp -r your_login@f-dahu:/bettik/your_login/your_folder folder_on_your_computer/` or `scp -r your_login@dahu.ciment:/bettik/your_login/your_folder folder_on_your_computer/`

- **Submit simple jobs on Gricad cluster**: Once logged into the cluster, do not run your code directly, you need to submit jobs:
1. Test an interactive job (https://gricad-doc.univ-grenoble-alpes.fr/en/hpc/joblaunch/job_management/#submitting-an-interactive-job) using `oarsub -I --project ml-remotesens` (on istoar, use --project iste-equ-cycle). Then run `ipython` to test if you can load all the libraries that you need, etc. For this, once you are logged into the node, first do for ex.:

     `source /applis/environments/conda.sh`

     `conda activate GPU`
2. Test to submit a job from a submission file using `oarsub -S ./test_helloworld_CPU_cluster.sh` (https://gricad-doc.univ-grenoble-alpes.fr/en/hpc/joblaunch/job_management/#submitting-a-job-via-a-submission-script) that points for ex. to a simple 'hello world' python file. See below _test_helloworld_CPU_cluster.sh_ and _test_helloworld.py_ (! the #OAR are not commented lines. change your_login)
3. If you need it, test to submit GPU jobs using `oarsub -S -t gpu ./test_helloworld_GPU_cluster.sh` (https://gricad-doc.univ-grenoble-alpes.fr/en/hpc/joblaunch/job_gpu/). Use the examples files _test_helloworld_GPU_cluster.sh_ and _test_helloworld_GPU.py_ below.


*test_helloworld.py*

`import numpy as np`

`print('hello world')`


*test_helloworld_CPU_cluster.sh*

`#!/bin/bash`

`## === 2. List of arguments ===`

`#OAR -l /nodes=1/core=1,walltime=00:30:00`

`#OAR -n test_python`

`#OAR --stdout /home/your_login/job_outputs/job.%jobid%.out`

`#OAR --stderr /home/your_login/job_outputs/job.%jobid%.err`

`#OAR --project ml-remotesens`

`## === 3. Purge and load needed modules ===`

`source /applis/environments/conda.sh`

`conda activate GPU`

`## === 5. Running the program ===`

`python3 -u /home/your_login/test_helloworld.py`


*test_helloworld_GPU.py*

`import torch`

`train_on_gpu = torch.cuda.is_available()`

`print('gpu active:'+str(train_on_gpu))`

`print('hello world')`


*test_helloworld_GPU_cluster.sh*

`#!/bin/bash`

`## === 2. List of arguments ===`

`## add -t gpu when you launch it!`

`#OAR -l /nodes=1/gpudevice=1/core=1,walltime=6:00:00`

`#OAR -n test_GPU_python`

`#OAR --stdout /home/your_login/job_outputs/job.%jobid%.out`

`#OAR --stderr /home/your_login/job_outputs/job.%jobid%.err`

`#OAR --project ml-remotesens`

`## === 3. Purge and load needed modules ===`

`source /applis/environments/conda.sh`

`conda activate GPU`

`## === 5. Running the program ===`

`python -u /home/your_login/test_helloworld_GPU.py`




